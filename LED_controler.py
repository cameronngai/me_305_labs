# -*- coding: utf-8 -*-
"""
Created on Fri Oct 16 09:17:25 2020

@author: camer
"""
import pyb

class LED_Controler:
    OFF = 0
    ON = 1
    BAUD = 115200
    
    def __init__(self,refresh_rate,led):
        ## Object representing the LED
        self.led = led
        ## Rate at which the states are checked and refreshed
        self.rate = refresh_rate
        
        # Initialize time parameters
        ## Timestamp of initialization
        self.init_time = pyb.micros()/10**6
        ## Current time of the task
        self.curr_time = self.init_time
        ## Next time at which the state is checked and refreshed
        self.refresh_time = self.curr_time+self.rate
        ## Next time at which the next pattern is used
        self.next_time = self.curr_time+self.rate
        
        # Settup channel
        self.uart = pyb.UART(2)#,self.BAUD)
        #self.uart.init(self.BAUD, bits=8, parity=None, stop=1)
        self.state = self.ON
        
        self.out = 1
        
        print('Press 0 to turn LED off')
        
    def run(self):
        self.curr_time = pyb.micros()/10**6
        if self.curr_time>self.next_time:
            self.next_time += self.rate
            char = self.uart.readchar()
            if self.state == self.OFF:
                if char == ord('1'):
                    self.out = 1
                    self.state = self.ON
                    print('Press 0 to turn LED off')
            elif self.state == self.ON:
                if char == ord('0'):
                    self.out = 0
                    self.state = self.OFF
                    print('Press 1 to turn LED on')
        self.led.pulse_width_percent(100*self.out)
        

if __name__ == '__main__':
    
    ## Refresh rate for both tasks
    refresh_rate = 0.05
    
    ## Pin for the LED
    pin5 = pyb.Pin(pyb.Pin.cpu.A5)
    ## Timer for the LED signal
    t = pyb.Timer(2,freq = 20000)
    ## signal for the LED
    led = t.channel(1,pyb.Timer.PWM,pin = pin5)
    
    ## Instance of task 2
    t2 = LED_Controler(refresh_rate,led)
    
    
    # Run Simulation
    ## Time for the program to run
    stop_time = 60
    while (t2.curr_time < t2.init_time+stop_time):
        t2.run()
    print('times up')