# -*- coding: utf-8 -*-
"""
Created on Wed Sep 23 12:54:35 2020

@author: camer
"""

def add(a,b):
    while a&b:
        a1 = a^b
        b = (a&b) << 1 
        a = a1
    a = a^b
    return a