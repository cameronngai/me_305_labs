## @file fibonacci.py
#  @package fibonacci
#
#  The fibonacci function will will output fibonacci numbers corresponding 
#  to inputted indexes. This file also contains a tester for the fibonacci 
#  function
#
#  Link to code: https://bitbucket.org/cameronngai/me_305_labs/src/master/Lab1/
#
#  @author Cameron Ngai
#
#  @date September 22, 2020
#
#  The fibonacci function will will output fibonacci numbers corresponding 
#  to inputted indexes. This file also contains a tester for the fibonacci 
#  function.
#
#  @author Cameron Ngai
#
#  @date September 22, 2020

def fib(n):
    ## A fibonacci number generator
    #
    #  This function will output the fibonacci number corresponding to the 
    #  inputted index. Any lists will be split into individual indexes for
    #  evaluation.
    #
    #  @author Cameron Ngai
    #  @date September 22, 2020
    print('Calculating Fibbanacci number at '
              'index n = {:}'.format(n))
    ##  Statement of what fibonacci number is being calculated
    if isinstance(n,list):
        ## Breaks up lists into individual indexes
        for index in n:
            fib(index)
    elif n == str(n):
        ## Rejects strings
        print('Strings do not quallify as fibonacci indices. Try inputting ' 
              'positive integers\n')
    elif n < 0:
        ## Rejects negative numbers
        print('This function does not include generalizations of the fibonacci' 
              ' series to negative numbers. Try inputting positive integers\n')
        return 'N/A'
    elif n != int(n):
        ## Rejects non-integers
        print('This function does not include generalizations of the fibonacci' 
              ' series to all real numbers. Try inputting positive integers\n')
    else:
        ## Computes fibonacci number for appropriate index
        i = 0
        f0 = 1
        f = 0
        while i < n:
            f00 = f0
            f0 = f
            f += f00
            i += 1
        print('The fibbanacci number at index {:}'.format(n)+' is:\n{:}'.format(f)+'\n')

if __name__ == '__main__':
    ##  Test for the fibonacci number generator
    #
    #  This will test the fibonacci number generator by inputting multiple
    #  sequential integers to assess accuracy, a large number to assess speed,
    #  incompatable numbers to assess rejection, and a list to assess the list
    #  break up feature.
    #  @author Cameron Ngai
    #  @date September 22, 2020
    #testers = [0,1,2,3,4,5,6,1000, -1, 0.5,'test', [0, 1, 2]]
    #for index in testers:
    #    fib(index)
    
    while True:
        f = fib(input('Enter an index to find the associated fibonacci number or exit program: '))