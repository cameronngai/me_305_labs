"""
@file   Lab2.py
@package Lab2
@brief  An excersise in performing multiple tasks on a microcontroller
@details    This file contains two main tasks for a microcontroller to use.
            One task turns on and off a virtual LED, and another controlls the 
            behavior of a LED in accordance to specified patterns.
            
            Link to code: https://bitbucket.org/cameronngai/me_305_labs/src/master/Lab2/Lab2.py

@author: Cameron Ngai
@date: October 7, 2020

"""
import pyb
from math import sin, pi

class Task_1:
    '''
    @brief  A task that turns on and off a virtual LED
    @details    This object will print statements declaring a virtual LED to be
                on or off. The virtual LED switches on and off after a specified
                interval time
    The finite state machine for this task is as below:
    @image html fsmLab2Task1.png
    '''
    
    # States
    ## Initialisation state
    INIT = -1
    ## State when the LED is off
    LED_OFF = 0 
    ## State when the LED is on
    LED_ON = 1
    
    def __init__(self,interval,refresh_rate):
        '''
        @brief  Creates a Task 1 object
        @param  interval  An object that represents the time interval between
                toggling the LED
        @param  refresh_rate  An object that reprensents the time interval between
                checking for updatting states
        '''
        # Internalize variables
        ## Interval between toggling the LED state
        self.interval = interval
        ## Rate at which the states are checked and refreshed
        self.rate = refresh_rate
        
        # Initialize time parameters
        ## Timestamp of initialization
        self.init_time = pyb.micros()/10**6
        ## Current time of the task
        self.curr_time = self.init_time
        ## Next time at which the state is checked and refreshed
        self.refresh_time = self.curr_time+self.rate
        ## Next time at which the LED must toggle
        self.next_time = self.curr_time+self.interval
        
        # Set initial state
        ## State of the task
        self.state = self.INIT
        
    def run(self):
        '''
        @brief Runs one iteration of the task
        '''
        # Set current time
        self.curr_time = pyb.micros()/10**6
        
        # Check for refresh time
        if self.curr_time >= self.refresh_time:
            # Update refresh time
            self.refresh_time += self.rate
            # Check for initial state
            if self.state == self.INIT:
                # Turn of LED
                self.transitionTo(self.LED_OFF)
            # Check for LED off state
            elif self.state == self.LED_OFF:
                if self.curr_time >= self.next_time:
                        # When interval is reached, turn LED on
                        self.transitionTo(self.LED_ON)
                        print('Virtual LED: ON')
                        # Set next interval completion time
                        self.next_time += self.interval
            # Check for LED off state
            elif self.state == self.LED_ON:
                if self.curr_time >= self.next_time:
                        # When interval is reached, turn LED on
                        self.transitionTo(self.LED_OFF)
                        print('Virtual LED: OFF')
                        # Set next interval completion time
                        self.next_time += self.interval
                
                
                
        
    def transitionTo(self,newState):
        '''
        @brief      Updates the variable defining the next state to run
        '''
        self.state = newState
        

class Task_2:
    '''
    @brief  A task that controlls the behavior of a LED in accordance to specified patterns
    @details    This object cycle through specified patterns for the LED to behave
                in accordance with at a sepecified time interval.
    The finite state machine for this task is as below:
    @image html fsmLab2Task2.png
    '''
    # Nontrivial states
    ## Initialization state
    INIT = -1
    ## First pattern state
    FIRST = 0
    
    def __init__(self,patterns,interval,refresh_rate,led):
        '''
        @brief  Creates a Task 1 object
        @param  patterns  A list of patterns to switch through
        @param  interval  An object that represents the time interval between
                switching between LEDs
        @param  refresh_rate  An object that reprensents the time interval between
                checking for updatting states
        @param  led  The LED that is controlled
        '''
        # Internalize variables
        ## List of patterns available
        self.patterns = patterns
        ## Total number of patterns available
        self.num_patterns = len(patterns)
        ## Interval of time that each pattern persists
        self.interval = interval
        ## Object representing the LED
        self.led = led
        ## Rate at which the states are checked and refreshed
        self.rate = refresh_rate
        
        # Initialize time parameters
        ## Timestamp of initialization
        self.init_time = pyb.micros()/10**6
        ## Current time of the task
        self.curr_time = self.init_time
        ## Next time at which the state is checked and refreshed
        self.refresh_time = self.curr_time+self.rate
        ## Next time at which the next pattern is used
        self.next_time = self.curr_time+self.interval
        
        
        # Set initial state
        ## State of the task
        self.state = self.INIT          

    def run(self):
        '''
        @brief Runs one iteration of the task
        '''
        # Set current time
        self.curr_time = pyb.micros()/10**6
        
        # Check for refresh time
        if self.curr_time >= self.refresh_time:
            # Set next refresh time
            self.refresh_time += self.rate
            
            # Check for initial state
            if self.state == self.INIT:
                self.transition()
                ## The pattern that is currently active
                self.Vout = self.patterns[self.FIRST]
                self.transition()
            # Check for unspecified state
            elif self.state >= self.num_patterns:
                # Set state to first pattern
                self.state = self.FIRST
            # Check for states corresponding to individual patterns
            for n in range(self.num_patterns):
                if self.state == n:
                    if self.curr_time >= self.next_time:
                        # When interval is reached switch state to next pattern
                        # and set set output to the corresponding pattern
                        self.next_time += self.interval
                        self.transition()
                        self.Vout = self.patterns[n]
                        # Set next transition time
                        self.next_time += self.interval
            # Output correct pattern
            self.led.pulse_width_percent(100*self.Vout.out())
        
    def transition(self):
        '''
        @brief      Updates the variable defining the next state to run
        '''
        self.state += 1

class Normal_Periodic_Pattern:
    '''
    @brief  An object that outputs a periodic pattern over time
    @details    This object will repeat a pattern with a specified period
    '''
    def __init__(self,period,function):
        
        # Internalize variables
        
        ## The period of the function
        self.period = period
        ## The function considered, which may be dependent on the period
        self.function = function
        
        # Initialize time parameters
        ## Timestamp of last pattern reset
        self.init_time = pyb.micros()/10**6
        ## Current time of the task
        self.curr_time = self.init_time
        ## Next time at which the pattern is set
        self.next_time = self.curr_time+self.period
        
    def out(self):
        '''
        @brief  This method outputs the value in accordance with the pattern
        '''
        self.curr_time = pyb.micros()/10**6
        ## Time since the last pattern reset
        self.t = self.curr_time-self.init_time
        
        if self.t >= self.period:
            # When time exceeds period, restart time
            self.init_time = self.curr_time
            self.t = 0
        
        # Return function at the proper time
        return self.function(self.t,self.period)
    
def func_1(t,period):    
    '''
    @brief  A function representing a sine wave
    '''
    return  1/2*(1+sin(t*2*pi/period))

def func_2(t,period):
    '''
    @brief  A function representing a rising line
    @details    This function will output a rising line that reaches 1 after a 
                specified period
    '''
    return   t/period
        

if __name__ == '__main__':
    '''
    @brief      Runs both tasks simultaniously
    @details    This is a test for the code. It runs both tasks simultaniously
    '''
    
    ## Virtual LED toggle interval for task 1
    interval1 = 7
    ## LED pattern switch interval for task 2
    interval2 = 30
    ## Refresh rate for both tasks
    refresh_rate = 0.05
    ## Period of each pattern for task 2
    period = 10
    
    ## Pin for the LED
    pin5 = pyb.Pin(pyb.Pin.cpu.A5)
    ## Timer for the LED signal
    t = pyb.Timer(2,freq = 20000)
    ## signal for the LED
    led = t.channel(1,pyb.Timer.PWM,pin = pin5)
    
    ## First pattern for task 2    
    pattern_1 = Normal_Periodic_Pattern(period,func_1)
    ## Second pattern for task 2
    pattern_2 = Normal_Periodic_Pattern(period,func_2)
    
    ## Instance of task 1
    t1 = Task_1(interval1,refresh_rate)
    ## Instance of task 2
    t2 = Task_2([pattern_1,pattern_2],interval2,refresh_rate,led)
    
    
    # Run Simulation
    ## Time for the program to run
    stop_time = 60
    while (t1.curr_time < t1.init_time+stop_time):
        t1.run()
        t2.run()

            
                
    
    

        
