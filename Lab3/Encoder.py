"""
@file   Encoder.py
@package Encoder
@brief  A set of objects for the opertaion of a controller
@details    This file contains a raw encoder object, and encoder object, and
            an interface for the encoder. The raw encoder object records encoder
            values without rejecting the effects of overflow and underflow. The
            encoder records the corrected values. The user interface allows a user
            to interact with the encoder.
            
            Link to code: https://bitbucket.org/cameronngai/me_305_labs/src/master/Lab3/Encoder.py

@author: Cameron Ngai
@date: October 17, 2020
"""
#import time
import pyb

class Raw_Encoder:
    '''
    @brief  An object that holds and gives raw information about the encoder
    @details    This object will track the raw position of the encoder. The tracked
                position is raw because this objoect will not reject the result 
                of overflow or underflow. This object will also give the difference
                between the current position and the previous position.
    '''
    ## The count at which the encoder will overflow and reset to zero
    OVERFLOW = 0XFFFF
    
    def __init__(self,timer,pinA,pinB):
        '''
        @brief  Creates a raw encoder
        @param  timer  The timer classification number
        @param  pinA  The pin for the first encoder input
        @param  pinB  The pin for the second encder input
        '''
        # Setup timer
        ## The timer that will count the encoder position
        self.tim = pyb.Timer(timer) 
        self.tim.init(prescaler = 0, period = self.OVERFLOW)
        self.tim.channel(1,mode = pyb.Timer.ENC_AB,pin=pinA)
        self.tim.channel(2,mode = pyb.Timer.ENC_AB,pin=pinB)

        # Setup count holders 
        ## The current count of the encoder
        self.curr_count = 0
        ## The previous count of the encoder
        self.prev_count = 0
        
    def update(self):
        '''
        @brief  Updates the current and previous count values
        '''
        self.prev_count = self.curr_count
        self.curr_count = self.tim.counter()  
        
    def get_position(self):
        '''
        @brief  Returns the current count
        '''
        return self.curr_count
    def set_position(self, pos_set):
        '''
        @brief  Sets the count to a certain value
        @param  pos_set  The position to set the count to

        '''
        # Check for if the position is outside the available count range, and
        # recursivaly find the corresponding count
        if pos_set < 0:
            self.set_position(pos_set+self.OVERFLOW)
        elif pos_set > self.OVERFLOW:
            self.set_position(pos_set-self.OVERFLOW)
        else:
            # Set all relevant count values to the set position
            self.curr_count = pos_set
            self.prev_count = pos_set
            self.tim.counter(pos_set)
        
    def get_delta(self):
        '''
        @brief  Returns the difference between the current count and the count
                of the previous update
        '''
        return self.curr_count-self.prev_count
    
    
class Encoder:
    '''
    @brief An object that tracks the position of the encoder
    @details    This object will use a raw encoder object to track the postion
                of the encoder by correcting for the effects of overflow and
                underflow.
     The finite state machine for this task is as below:
    @image html fsmEncoder.png
    '''
    # States
    ## Initialization state
    INIT = 0
    ## State of checking if the differential is correct
    CHECK_DELTA = 1
    ## State of correcting the differential if overflow occurrs
    OVERFLOW_DELTA = 2
    ## State of correcting the differential if underflow occurrs
    UNDERFLOW_DELTA = 3
    
    ## The count at which the encoder will overflow and reset to zero
    OVERFLOW = 0XFFFF
    
    CONV = 60/40000
    def __init__(self,encoder,interval):
        '''
        @brief  Creates an encoder object
        @param  encoder  The raw encoder to track
        @param  interval  The update interval
        '''
        # Internalize inputs
        ## The raw encoder to track
        self.encoder = encoder
        ## The update interval
        self.interval = interval
        
        # Initialize time checks
        ## Startup time
        self.init_time = pyb.micros()/10**6
        ## Current time
        self.curr_time = self.init_time
        ## Timestamp for update
        self.next_time = self.curr_time+self.interval
        
        ## State of encoder
        self.state = self.INIT
        self.delta = 0
    def update(self):
        '''
        @brief  Updates the encoder position
        '''
        # Set current time
        self.curr_time = pyb.micros()/10**6
        # Update when the update timestamp is reached
        if self.curr_time>self.next_time:
            # Set next update timestamp
            self.next_time += self.interval
            
            # Run states
            if self.state == self.INIT:
                # Initialize encoder
                ## Tracked position of encoder
                self.position = 0
                ## Differential between previous encoder reading and current encoder reading
                self.delta = 0
                #transition
                self.state = self.CHECK_DELTA
            elif self.state == self.CHECK_DELTA:
                # update raw encoder information
                self.encoder.update()
                self.delta = self.encoder.get_delta()
                
                # Check for overflow and underflow
                if self.delta < -self.OVERFLOW/2:
                    self.state = self.OVERFLOW_DELTA
                elif self.delta > self.OVERFLOW/2:
                    self.state = self.UNDERFLOW_DELTA
                    
            if self.state == self.OVERFLOW_DELTA:
                # Correct differential for overflow
                self.delta += self.OVERFLOW
                self.state = self.CHECK_DELTA
            elif self.state == self.UNDERFLOW_DELTA:
                # Correct differential for underflow
                self.delta -= self.OVERFLOW
                self.state = self.CHECK_DELTA
            
            self.position += self.delta
    def zero(self):
        '''
        @brief  Resets encoder position to zero
        '''
        self.state = self.INIT
        self.encoder.set_position(0)
    def get_position(self):
        '''
        @brief  Returns the current count
        '''
        return self.position
    def get_delta(self):
        '''
        @brief  Returns the difference between the current position and the previous
                position
        '''
        return self.delta
    
    def get_vel(self):
        return self.delta/self.interval*self.CONV

class Encoder_Interface:
    '''
    @brief  An object allowing the user to interface the encoder
    @details    This object will zero the encoder, get the encoder position, 
                and get the change in encoder position between updates
    The finite state machine for this task is as below:
    @image html fsmEncoder.png
    '''
    # Interface states
    ## Initialize interface
    INIT = 0
    ## Print the available commands
    PRINT_COMMANDS = 1
    ## Wait to recieve user commands
    GET_COMMANDS = 2
    ## Execute user commands
    RESPOND = 3
    
    ## Signiture of no user response
    NULL_CHAR = -1
    
    def __init__(self,interval):
        '''
        @brief  Creates an interface object
        @param  interval  The interval between updating the interface
        '''
        # Internalize variables
        ## Interval between update
        self.interval = interval
        
        ## Interface channel
        self.uart = pyb.UART(2)
        
        
        # Initialize time checks
        ## Startup time
        self.init_time = pyb.micros()/10**6
        ## Current time
        self.curr_time = self.init_time
        ## Timestamp for update
        self.next_time = self.curr_time+self.interval
        
        ## State of the interface
        self.state = self.INIT
        
        ## A boolian representing whether the program should terminate
        self.EXIT = False
        
    def update(self,encoder):
        '''
        @brief  Runs the user interface for the specified encoder
        @param  encoder  The encoder to interface with
        '''
        # Set current time
        self.curr_time = pyb.micros()/10**6
        
        # Update when update timestamp is reached
        if self.curr_time>self.next_time:
            # Set next update timestamp
            self.next_time += self.interval
            
            # Run states
            if self.state == self.INIT:
                # Transition
                self.state = self.PRINT_COMMANDS
                self.char = 0
                
            elif self.state == self.PRINT_COMMANDS:
                # Print commands
                print('Please enter one of the following commands: \n'
                      'z: Zero encoder\n'
                      'p: Get encoder position\n'
                      'd: Get encoder delta\n'
                      'e: Exit\n')
                # Transition
                self.state = self.RESPOND
            elif self.state == self.GET_COMMANDS:
                # Read interface
                self.char = self.uart.readchar()
                # Check for unempty read and transition
                if self.char != self.NULL_CHAR:
                    self.state = self.PRINT_COMMANDS
            if self.state == self.RESPOND:
                # Execute commands
                if self.char == ord('z'):
                    encoder.zero()
                    print('Encoder zeroed \n')
                elif self.char == ord('p'):
                    print('Encoder position: '+str(encoder.get_position())+'\n')
                elif self.char == ord('d'):
                    print('Encoder delta: '+str(encoder.get_delta())+'\n')
                elif self.char == ord('e'):
                    self.EXIT = True
                # Transition
                self.state = self.GET_COMMANDS


if __name__ == '__main__':
    '''
    @brief      Runs the encoder with an interface
    @details    This is a test for the code. It lets the user interface with the
                encoder
    @author:     Cameron Ngai
    @date: October 17, 2020
    '''
    
    ## Timer type for encoder
    timer = 4
    
    ## First encoder input pin
    pinA = pyb.Pin.cpu.B6  # Digital 10
    ## Second encoder input pin
    pinB = pyb.Pin.cpu.B7
    
    ## update interval
    interval = 0.01
    
    ## Instance of incoder
    enc = Encoder(Raw_Encoder(timer,pinA,pinB),interval)
    ## Instance of interface
    interface = Encoder_Interface(interval)
    
    enc.zero()
    
    # Run Simulation
    while (interface.EXIT == False):
        enc.update()
        interface.update(enc)
        