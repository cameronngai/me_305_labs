'''
@file   Encoder_Test.py
@package Encoder_Test
@brief      Runs the encoder with an interface
@details    This is a test for the code. It lets the user interface with the
            encoder
            Link to code: https://bitbucket.org/cameronngai/me_305_labs/src/master/Lab3/Encoder_Test.py
@author:     Cameron Ngai
@date: October 17, 2020
'''
import pyb
from Encoder import Raw_Encoder,Encoder, Encoder_Interface

## Timer type for encoder
timer = 3

## First encoder input pin
pinA = pyb.Pin.cpu.A6
## Second encoder input pin
pinB = pyb.Pin.cpu.A7

## update interval
interval = 0.01
  
## Instance of incoder
enc = Encoder(Raw_Encoder(timer,pinA,pinB),interval)
## Instance of interface
interface = Encoder_Interface(interval)
   
# Run Simulation
while (interface.EXIT == False):
    enc.update()
    interface.update(enc)