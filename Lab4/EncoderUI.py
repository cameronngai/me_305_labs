# -*- coding: utf-8 -*-
"""
@file  EncoderUI.py
@package EncoderUI
@brief      The computer end of a user interface for an encoder
@details    This file contains the computer end of a user interface for an encoder. It works in conjunction with 
            NucleoEncoderUI.py, which manages the interface that the Nucleo interacts with.
            This interaction is shown in the diagram below:
@image html EncoderUITask.png
            Link to code: https://bitbucket.org/cameronngai/me_305_labs/src/master/Lab4/EncoderUI.py
@author:     Cameron Ngai
@date: October 31, 2020
"""

import serial
import matplotlib as mpl
import time
import sys
import msvcrt
import numpy as np

class Encoder_UI:
    '''
    @brief  An object allowing the user to interface the encoder
    @details    This object will allow the user to collect data from the encoder.
                The data will be downloaded as both a list and an excell file.
                The user interface is designed to be cooperative with other tasks.
                However, some consoles are not compatible with this cooperative
                interface. If the console does not allow user input, the user
                must change the console to be the default computer command line.
                This can be accomplished under the tools tab by sellecting preferences.
                Under the run tab, set the console setting to "execute in an external
                system terminal"
                
    The finite state machine for this task is as below:
    @image html EncoderUI.png
    '''
    # Interface states
    ## Initialize interface
    INIT = 0
    ## Print the available commands
    PRINT_COMMANDS = 1
    ## Wait to recieve user commands
    GET_COMMANDS = 2
    ## Execute user commands
    RESPOND = 3
    ## Collect Data
    COLLECT = 4
    ## Load time array
    GET_TOUT = 5
    ## Load position array
    GET_XOUT = 6
    ## Publish data
    PUBLISH = 7

    
    def __init__(self,interval,runtime):
        '''
        @brief  Creates an interface object
        @param  interval  The interval between updating the interface
        @param  runtime  The time interval of data collection
        '''
        # Internalize variables
        ## Interval between update
        self.interval = interval
        ## Data colletion time
        self.runtime = runtime
        
        ## Interface channel
        self.ser = serial.Serial(port='COM5',baudrate=115273,timeout=1)
        
        # Initialize time checks
        ## Startup time
        self.init_time = time.time()
        ## Current time
        self.curr_time = self.init_time
        ## Timestamp for update
        self.next_time = self.curr_time+self.interval
        
        ## State of the interface
        self.state = self.INIT
        
        ## A boolian representing whether the program should terminate
        self.EXIT = False
        
    def update(self):
        '''
        @brief  Runs the user interface for the specified encoder
        '''
        # Set current time
        self.curr_time = time.time()
        
        # Update when update timestamp is reached
        if self.curr_time>self.next_time:
            # Set next update timestamp
            self.next_time += self.interval
            
            # Run states
            if self.state == self.INIT:
                # Transition
                self.state = self.PRINT_COMMANDS
                ## Boolian representing whether data has been collected
                self.collected = False
                ## Character inputted into the interface
                self.char = None
                
            elif self.state == self.PRINT_COMMANDS:
                # Print available commands
                print('Please enter one of the following commands: \n'
                      'g: Begin collecting data\n'
                      'p: Stop collecting data\n'
                      'e: Exit\n')
                # Transition
                self.state = self.RESPOND
            elif self.state == self.GET_COMMANDS:
                # Read interface
                (self.char,end) = coopinput(self.char)
                # transition
                if end:
                    self.reset_menu()
                
            if self.state == self.RESPOND:
                # Transition
                self.state = self.GET_COMMANDS
                
                # Execute commands
                if self.char == 'g':
                    self.send_command('g')
                    self.state = self.COLLECT
                    ## Time at which data collection will end
                    self.stoptime = self.curr_time + self.runtime
                    print('Data collection running')
                elif self.char == 's':
                    if self.collected:
                        print('Data collected')
                    else:
                        print('No data collection to stop')
                elif self.char == 'e':
                    self.EXIT = True
                elif self.char == None:
                    print('')
                else: 
                    print('Invalid Command')
                print('\nInput Command: ', end ='')
                
                # Refresh
                sys.stdout.flush()
                self.char = ''
                
            if self.state == self.COLLECT:
                # Get input
                (self.char,end) = coopinput(self.char)
                # Stop if s is inputed
                if end:
                    if self.char == 's':     
                        self.stop_collection()
                    else:
                        print('\nWait for data to finnish collecting or press s to stop')
                        print('\nInput Command: ',end = '')
                        sys.stdout.flush()
                # Stop if data collection time is passed
                if self.curr_time > self.stoptime:
                    print('\n')
                    self.stop_collection()
    
            if self.state == self.GET_TOUT: 
                # Update TOUT array
                (self.tout,done) = self.update_array(self.tout,'t',4)
                if done:
                    self.state = self.GET_XOUT
                
            if self.state == self.GET_XOUT:
                # Update XOUT array
                self.send_command('x')
                (self.xout,done) = self.update_array(self.xout,'x',2)
                if done:
                    self.state = self.PUBLISH
                    
            if self.state == self.PUBLISH:
                
                # Process arrays
                self.tout = self.tout/10**6
                self.xout = self.reject_overflow(self.xout,0XFFFF)
                
                # Plot arrays
                mpl.use('AGG')
                mpl.pyplot.plot(self.tout,self.xout)
                mpl.pyplot.xlabel('time, t [s]')
                mpl.pyplot.ylabel('position, x [m]')
                mpl.pyplot.savefig('plotted.png')
                
                np.save('tout.npy',self.tout)
                np.save('xout.npy',self.xout)
                
                # Return to menu
                self.collected = True
                self.reset_menu()
                
                
    def send_command(self,command):
        '''
        @brief  Sends command through serial
        @param  command  The command sent through the serial    
        '''
        self.ser.write(str(command).encode('ascii'))
        
    def get_command(self):
        '''
        @brief  gets command through serial    
        '''
        return self.ser.readline().decode('ascii')
    
    def update_array(self,curr_array,comm,expbyte):
        '''
        @brief  Updates array sent through serial
        @param  curr_array  Current array to be updated
        @param  comm    Command to send through the serial to coordinate
                        array transfer
        @param  expbyte     Expected number of bytes per array object
        '''
        if self.first:               
            # Request data
            self.send_command(comm)
            ## Boolian representing whether the first element of the array is being sent
            self.first = False
        if self.ser.in_waiting >= expbyte+1:
            # Get data
            b = bytearray(self.ser.read(size = expbyte+1))
            raw_array = np.array(b)
            bytrat = len(raw_array)
            
            # Prepair array
            array = curr_array
            l = len(array)
            array = np.append(array,[0])
            
            # Assess if the object is the last object of the array
            if raw_array[bytrat-1] == 0:
                done = False
                self.send_command(comm)
            else:
                done = True
                self.first = True
                
            # convert data from bytes
            for j in range(expbyte):
                array[l] += raw_array[j]*(2**8)**j
        
        else:
            # Keep array if no data recieved
            done = False
            array = curr_array
        return (array,done)
    
    def stop_collection(self):
        '''
        @brief  Stops collection of data and begins recieving data from the serial   
        '''
        self.char = 's'
        self.state = self.GET_TOUT
        self.send_command('s')
        ## Array holding time data
        self.tout = np.array([])
        ## Array holding position data
        self.xout = np.array([])
        self.first = True
        
    def reset_menu(self):
        '''
        @brief  Resets the menu of commands 
        '''
        self.state = self.PRINT_COMMANDS
        print('_____________________________________________')
        print('\r')
        
    def reject_overflow(self,raw_array,overflow):
        '''
        @brief  Sends command through serial
        @param  raw_array   The array that requires overflow correction
        @param  overflow    The value at which overflow occurs
        '''
        l = raw_array.size
        array = np.array([0]*l)
        for i in range(1,l):
            delta = raw_array[i]-raw_array[i-1]
            if delta > overflow/2:
                delta -= overflow+1
            elif delta < -overflow/2:
                delta += overflow+1
            array[i] = array[i-1] + delta
        return array
            
            

def coopinput(line):
    '''
    @brief  A function that allows the user to cooperatively interact with the UI
    @details    This function constructs a string as the user inputs charactors 
                and submits the string when the user presses enter. This allows
                the computor to perform actions while the user provides input
    @param  line    The string currently being inputed into the interface
    '''
    user_input = ''
    end = False
    if msvcrt.kbhit():
        user_input = msvcrt.getch()
        if user_input== b'\r':
            end = True
            user_input = ''
            print('\r')
        else:
            user_input = user_input.decode('utf-8')
            print(user_input,end = '')
            sys.stdout.flush()
    newline = line+user_input
    return (newline, end)
    

if __name__ == '__main__':
    '''
    @brief  Runs an instance of the encoder interface for testing
    '''
    ## The interval between updates
    interval = 0.001
    ## The time spent collecting data
    runtime = 5
    
    ## The encoder interface
    e = Encoder_UI(interval, runtime)
    
    # Run program
    while e.EXIT == False:
        e.update()