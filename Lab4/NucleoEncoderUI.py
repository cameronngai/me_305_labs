'''
@file  NucleoEncoderUI.py
@package NucleoEncoderUI
@brief      The Nucleo end of a user interface for an encoder
@details    This file contains the Nucleo end of a user interface for an encoder. It works in conjunction with 
            EncoderUI.py, which manages the interface that the user interacts with.
            This interaction is shown in the diagram below:
@image html EncoderUITask.png
            Link to code: https://bitbucket.org/cameronngai/me_305_labs/src/master/Lab4/NucleoEncoderUI.py
@author:     Cameron Ngai
@date: October 31, 2020
'''

import pyb
import array
from Encoder import Raw_Encoder as encoder

class NucleoUI:
    '''
    @brief  A task that turns on and off a virtual LED
    @details    This object will print statements declaring a virtual LED to be
                on or off. The virtual LED switches on and off after a specified
                interval time
    The finite state machine for this task is as below:
    @image html NucleoEncoderUI.png
    '''
    
    # States
    ## Initialisation state
    INIT = 1
    ## State when the interface is doing nothing
    STAND_BY = 2
    ## State when the encoder is collecting data
    COLLECT = 3
    ## State when the interface is sending data
    SEND = 4
    
    def __init__(self,encoder,refresh_rate):
        '''
        @brief  Creates a Task 1 object
        @param  encoder     The raw encoder to collect data from
        @param  refresh_rate  The rate at which the user interface updates itself
        '''
        # Internalize variables
        ## Raw encoder from which position data is derived
        self.encoder = encoder
        ## Rate at which the states are checked and refreshed
        self.rate = refresh_rate*10**6
        
        ## Serial connection with computor
        self.uart = pyb.UART(2)
        
        # Initialize time parameters
        ## Timestamp of initialization
        self.init_time = pyb.micros()
        ## Current time of the task
        self.curr_time = self.init_time
        ## Next time at which the state is checked and refreshed
        self.refresh_time = self.curr_time+self.rate
        
        # Set initial state
        ## State of the task
        self.state = self.INIT
        
        # LED signals when data is being collected
        # Pin for the LED
        pin5 = pyb.Pin(pyb.Pin.cpu.A5)
        # Timer for the LED signal
        t = pyb.Timer(2,freq = 20000)
        ## signal for the LED
        self.led = t.channel(1,pyb.Timer.PWM,pin = pin5)
        ## Output of the LED
        self.out = 0
        
        
    def run(self):
        '''
        @brief Runs the interface
        '''
        # Set current time
        self.curr_time = pyb.micros()
        
        # Check for refresh time
        if self.curr_time >= self.refresh_time:
            # Update refresh time
            self.refresh_time += self.rate
            # Check for initial state
            
            if self.state == self.INIT:
                self.state = self.STAND_BY
            elif self.state == self.STAND_BY:
                # Value sent to the interface from the serial
                val = self.get_command()
                if val == ord('g'):
                    # Begin collection
                    
                    # Zero encoder
                    self.encoder.set_position(0)
                    
                    # Set next state
                    self.state = self.COLLECT
                    
                    # Turn LED on
                    self.out = 1
                    
                    # Initiate arrays
                    ## Array of timestamps for the positions
                    self.tout = array.array('L',[])
                    ## Array of positions
                    self.xout = array.array('H',[])
                    
                    # Set start time
                    ## Time at which collection begins
                    self.start_time = self.curr_time
                    
            elif self.state == self.COLLECT:
                # Update arrays with data
                val = self.get_command()
                self.encoder.update
                self.tout.append(self.curr_time-self.start_time)
                self.xout.append(self.encoder.get_position())
                
                # Quit if stopped
                if val == ord('s'):
                    ## Index of array object to send
                    self.i = 0
                    self.state = self.SEND
            
        if self.state == self.SEND:
            val = self.get_command()
            if val == ord('t'):
                # Send tout array data
                self.send_array(self.tout,'L')
                if self.i >= len(self.tout):
                    self.i = 0
            if val == ord('x'):
                # Send xout array data
                self.send_array(self.xout,'H')
                if self.i >= len(self.xout):
                    # Finish collection
                    self.i = 0
                    #self.out = 0
                    del(self.tout)
                    del(self.xout)
                    self.state = self.INIT
        
        # Run LED
        self.led.pulse_width_percent(100*self.out)
        
    def get_command(self):
        '''
        @brief  gets command through serial    
        '''
        if self.uart.any() != 0:
            return self.uart.readchar()
        else:
            return ''
    def send_array(self, arr,typ):
        '''
        @brief  Sends array object through serial
        @param  arr     The array sent through the serial
        @param  tye     The type of array object
        '''
        # Assess if the object is the last object in the array
        if self.i == len(arr)-1:
            end = 1
        else:
            end = 0
            
        ## Bytearray representing object in array to send through the serial
        b = bytearray(array.array(typ,[arr[self.i]]))+bytearray([end])
        
        # Submit bytearray and reindex
        #if len(b) != 5:
        #    self.out = 0
        self.uart.write(b)
        self.i += 1 #n
    #def send_command(self, command):
     #   self.uart.write(str(command).encode('ascii'))


if __name__ == '__main__':
    '''
    @brief  Runs an instance of the encoder interface for testing
    '''
    ## Timer type for encoder
    timer = 3
    
    ## First encoder input pin
    pinA = pyb.Pin.cpu.A6  # Digital 10
    ## Second encoder input pin
    pinB = pyb.Pin.cpu.A7
    ## Raw encoder to collect data from
    e = encoder(timer,pinA,pinB)
    ## Interface on the nucleo
    ui = NucleoUI(e,0.01)
    
    # Run program
    while True:
        ui.run()
