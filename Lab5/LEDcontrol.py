# -*- coding: utf-8 -*-
"""
@file  LEDcontrol.py
@package  LEDcontrol
@brief      A method for blinking an LED
@details    This file contains both an object that blinks an LED and an object
            that interfaces the blinker with bluetooth commands, such that the
            blinking occurs at inputed frequencies.
            This interaction is shown in the diagram below:
@image html BlinkTask.PNG
            Link to code: https://bitbucket.org/cameronngai/me_305_labs/src/master/Lab5/LEDcontrol.py
@author:     Cameron Ngai
@date: November 8, 2020
"""

import pyb

class blink_interface:
    '''
    @brief  A task that interfaces bluetooth with a LED blinker
    @details    This object will recieve frequencies through bluetooth, and will
                set a variable to represent this frequency. It will also send
                confirmation of recieval through the same bluetooth line.
    The finite state machine for this task is as below:
    @image html BlinkInterface.PNG
    '''
    
    # States
    ## Initialisation state
    INIT = 1
    ## State when the interface is waiting for commands
    STAND_BY = 2
    ## State when the interface is processing the command
    CHECK_VAL = 3
    
    def __init__(self,refresh_rate):
        '''
        @brief  Creates an instance of the blink interface
        @param  refresh_rate  The rate at which the interface updates itself
        '''
        ## Rate at which the states are checked and refreshed
        self.rate = refresh_rate*10**6
        
        ## Serial connection with computor
        self.uart = pyb.UART(3)
        self.uart.init(9600)
        
        # Initialize time parameters
        ## Timestamp of initialization
        self.init_time = pyb.micros()
        ## Current time of the task
        self.curr_time = self.init_time
        ## Next time at which the state is checked and refreshed
        self.refresh_time = self.curr_time+self.rate
        
        # Set initial state
        ## State of the task
        self.state = self.INIT
        ## Blink frequency
        self.frequency = 0
        
    def run(self):
        '''
        @brief Runs the interface
        '''
        # Set current time
        self.curr_time = pyb.micros()
        
        # Check for refresh time
        if self.curr_time >= self.refresh_time:
            # Update refresh time
            self.refresh_time += self.rate
            # Check for initial state
            
            if self.state == self.INIT:
                self.state = self.STAND_BY
            elif self.state == self.STAND_BY:
                # Value sent to the interface from the serial
                if self.uart.any() != 0:
                    size1 = self.uart.any()
                    pyb.udelay(int(1/9500*15*10**6))
                    size2 = self.uart.any()
                    # Check if the system is done sending
                    if size1 == size2:
                        ## Raw value recieved through bluetooth
                        self.val = self.uart.read(self.uart.any())
                        print(self.val)
                        ## Track position of string element to assess
                        self.n = 0
                        ## Track length of string
                        self.l = len(self.val)
                        ## Track number of decimals
                        self.dec_count = 0
                        ## Track whether the string is a positive float
                        self.is_numb = True
                    
                        # Switch state to set the frequency
                        self.state = self.CHECK_VAL                   
                    
            elif self.state == self.CHECK_VAL:
                if self.n < self.l:
                    # Check for valid frequency
                    if (self.val[self.n] >= ord ('0')) & (self.val[self.n] <= ord('9')):
                        self.n += 1
                    elif self.val[self.n] == ord('.'):
                        self.dec_count += 1
                        self.n += 1
                        if self.dec_count > 1:
                            self.n = self.l
                            self.is_numb = False
                    else:
                        self.n = self.l
                        self.is_numb = False
                else:
                    # Set frequency
                    if self.is_numb:
                        # turn frequency to usable number
                        string = self.val.decode('ASCII')
                        print(('Frequency set to '+string+' Hz'))
                        self.uart.write(('Frequency set to '+string+' Hz').encode('ASCII'))
                        self.frequency = float(string)/10**6
                    else:
                        # Reject and set frequency to 0
                        print('Invalid Frequency')
                        self.uart.write('Invalid Frequency'.encode('ASCII'))
                        self.frequency = 0
                    # Change state
                    self.state = self.STAND_BY
                    
        
    def get_command(self):
        '''
        @brief  gets command through serial    
        '''
        if self.uart.any() != 0:
            return self.uart.readchar()
        else:
            return ''

class blink:
    '''
    @brief  A task that blinks an LED
    @details    This object will blink an LED at frequencies requested by an interface
    The finite state machine for this task is as below:
    @image html Blinker.PNG
    '''
    ## Initation state
    INIT = 1
    ## State when the LED is off
    LED_OFF = 2
    ## State when the LED is on
    LED_ON = 3
    ## State when frequency is zero
    NO_FREQ = 4
    
    def __init__(self,refresh_rate,interface):
        '''
        @brief  Creates an instance of the blinker
        @param  refresh_rate  The rate at which the blinker updates itself
        @param  interface   The interface that dictates frequencies to blink at
        '''
        ## The rate at which the blinker updates itself
        self.rate = refresh_rate
        ## The interface that dictates frequencies
        self.interface = interface
        
        # Initialize time parameters
        ## Timestamp of initialization
        self.init_time = pyb.micros()
        ## Current time of the task
        self.curr_time = self.init_time
        ## Next time at which the state is checked and refreshed
        self.refresh_time = self.curr_time+self.rate
        
        # Set state
        ## The state of the blinker
        self.state = self.INIT
        
        # LED parameters
        # Pin for the LED
        pin5 = pyb.Pin(pyb.Pin.cpu.A5)
        # Timer for the LED signal
        t = pyb.Timer(2,freq = 20000)
        ## signal for the LED
        self.led = t.channel(1,pyb.Timer.PWM,pin = pin5)
        ## Output of the LED
        self.out = 0
        
    def run(self):
        '''
        @brief Runs the blinker
        '''
        # Set current time
        self.curr_time = pyb.micros()
        # Run LED
        self.led.pulse_width_percent(100*self.out)  
        # Check for refresh time
        if self.curr_time >= self.refresh_time:
            # Update refresh time
            self.refresh_time += self.rate
            if self.state == self.INIT:
                # Initialize to no frequency state
                self.state = self.NO_FREQ
                self.out = 1
            elif self.state == self.NO_FREQ:
                if self.interface.frequency != 0:
                    # Start blinking if frequency is not 0
                    self.out = 0
                    ## Timestamp for next time to toggle
                    self.toggle = self.curr_time + 1/self.interface.frequency
                    self.state = self.LED_OFF
            elif self.state == self.LED_ON:
                if self.curr_time >= self.toggle:
                    # Toggle when timestamp is reached
                    if self.interface.frequency == 0:
                        # Go to no frequency if frequency is zero
                        self.out = 1
                        self.state = self.NO_FREQ
                    else:
                        # Set next toggle timestamp and toggle
                        self.toggle += 1/self.interface.frequency
                        self.out = 0
                        self.state = self.LED_OFF
                elif self.interface.frequency == 0:
                    # Go to no frequency if frequency is zero
                    self.out = 1
                    self.state = self.NO_FREQ
            elif self.state == self.LED_OFF:
                if self.curr_time >= self.toggle:
                    # Toggle when timestamp is reached
                    if self.interface.frequency == 0:
                        # Go to no frequency if frequency is zero
                        self.out = 1
                        self.state = self.NO_FREQ
                    else:
                        # Set next toggle timestamp and toggle
                        self.toggle += 1/self.interface.frequency
                        self.out = 1
                        self.state = self.LED_ON
                elif self.interface.frequency == 0:
                    # Go to no frequency if frequency is zero
                    self.out = 1
                    self.state = self.NO_FREQ

if __name__ == '__main__': 
    '''
    @brief  An example of the use of the interface and blinker
    '''           
    ## Interface for the blinker
    interface = blink_interface(0.01)
    ## Blinker
    bl = blink(0.01,interface)
    # Run program
    while True:
        interface.run()
        bl.run()