# -*- coding: utf-8 -*-
"""
@file   ClosedLoop.py
@package ClosedLoop
@brief  This file contains a closed loop controller for a motor
@details    This file contains an object that controlls a motor using a closed
            loop proportional method.
            The best proportional gain is Kp = 4. This fact can be seen by comparing
            it to nearby alternatives. Kp = 2 has a large steady state error.
@image html Kp2.png
            Kp = 6 has large noise.
@image html Kp6.png
            But Kp = 4 is nested neatly between both extremes.
@image html Kp4.png
@details    Link to code: https://bitbucket.org/cameronngai/me_305_labs/src/master/Lab6/ClosedLoop.py

@details    See NucleoUI.py for details on integration techneques.
            
            
@author: Cameron Ngai
@date: November 18, 2020
"""
import pyb
import MotorDriver as mo
import Encoder as enc

class ClosedLoop:
    '''
    @brief  A task that controls the speed of a motor
    @details    This object will apply a closed loop proportional controll
                scheme to a motor to controll the speed.
    '''
    
    def __init__(self,encoder,motor,refresh_rate):
        '''
        @brief  Creates a ClosedLoop object
        @param  encoder     The encoder used to access the actual speed
        @param  motor       The motor to controll
        @param  refresh_rate  The rate at which the Closed Loop system updates itself
        '''
        # Internalize variables
        ## Raw encoder from which position data is derived
        self.encoder = encoder
        self.motor = motor
        ## Rate at which the states are checked and refreshed
        self.rate = refresh_rate*10**6
        
        ## Serial connection with computor
        self.uart = pyb.UART(2)
        
        # Initialize time parameters
        ## Timestamp of initialization
        self.init_time = pyb.micros()
        ## Current time of the task
        self.curr_time = self.init_time
        ## Next time at which the state is checked and refreshed
        self.refresh_time = self.curr_time+self.rate    
        
        ## Desied velocity
        self.des_vel = 0
        ## Actual velocity
        self.act_vel = 0
        ## Proportional gain
        self.Kp = 0
        ## Maximum voltage
        self.Vdc = 12
        
    def run(self):
        '''
        @brief Runs the closed loop controller
        '''
        # Set current time
        self.curr_time = pyb.micros()
        
        # Check for refresh time
        if self.curr_time >= self.refresh_time:
            # Update refresh time
            self.refresh_time += self.rate
            
            # Get velocity
            self.act_vel = self.encoder.get_vel()
            
            # Set duty of motor.
            pulse = self.Kp/self.Vdc*(self.des_vel-self.act_vel)
            self.motor.set_duty(pulse*100)
                
                
        # Update encoder
        self.encoder.update()
        
    def get_Kp(self):
        '''
        @brief  Creates a ClosedLoop object
        '''
        return self.Kp
    def set_Kp(self,Kp):
        '''
        @brief  Creates a ClosedLoop object
        @param  Kp  The proportional gain to set.
        '''
        self.Kp = Kp
    def set_vel(self,vel):
        '''
        @brief  Creates a ClosedLoop object
        @param  vel  The desired velocity to set.
        '''
        self.des_vel = vel
        
if __name__ =='__main__':
    
    interval = 0.01
    # Create the pin objects used for interfacing with the motor drivers      
    pin_nSLEEP  = pyb.Pin(pyb.Pin.cpu.A15,pyb.Pin.OUT_PP)   
    pin_IN1     = pyb.Pin(pyb.Pin.cpu.B5)      
    pin_IN2     = pyb.Pin(pyb.Pin.cpu.B4)
    #pin_IN3     = pyb.Pin(pyb.Pin.cpu.B0)      
    #pin_IN4     = pyb.Pin(pyb.Pin.cpu.B1)
    
    # Create the timer object used for PWM generation
    tim = pyb.Timer(3,freq = 20000)
    # Create motor objects passing in the pins and timer
    moe     = mo.MotorDriver(pin_nSLEEP, pin_IN1, pin_IN2, tim)
    # Enable the motor driver
    moe.enable()
    
    moe.set_duty(80)
    timer = 4
    ## First encoder input pin
    pinA = pyb.Pin.cpu.B6  # Digital 10
    ## Second encoder input pin
    pinB = pyb.Pin.cpu.B7
    ## Raw encoder to collect data from
    re = enc.Raw_Encoder(timer,pinA,pinB)
    ## Encoder to use as feedback
    e = enc.Encoder(re,interval)
    
    ## Interface on the nucleo
    CL = ClosedLoop(e,moe,interval)
    
    CL.set_Kp(4)
    CL.set_vel(-15)
    
    while True:
        CL.run()
    
    