# -*- coding: utf-8 -*-
"""
@file   MotorDriver.py
@package MotorDriver
@brief  Driver for a motor
@details    This file contains the driver for a motor. It also includes a tester
            that initializes the motor
            
            Link to code: https://bitbucket.org/cameronngai/me_305_labs/src/master/Lab6/MotorDriver.py

@author: Cameron Ngai
@date: November 10, 2020

"""

import pyb

class MotorDriver:
    ''' @brief  This class implements a motor driver for the ME405 board. '''
    def __init__ (self, nSLEEP_pin, IN1_pin, IN2_pin, timer):
        ''' 
        @brief  Creates a motor driver by initializing GPIO12pins and turning the motor off for safety.13
        @param nSLEEP_pin   A pyb.Pin object to use as the enable pin.14
        @param IN1_pin      A pyb.Pin object to use as the input to half bridge 1.15
        @param IN2_pin      A pyb.Pin object to use as the input to half bridge 2.16
        @param timer        A pyb.Timer object to use for PWM generation on17IN1_pin and IN2_pin. 
        '''
        ## Enable and disable pin
        self.sleep = nSLEEP_pin
        
        # Find Channels for pins
        if (IN1_pin == pyb.Pin(pyb.Pin.cpu.B4))&(IN2_pin == pyb.Pin(pyb.Pin.cpu.B5)):
            ch1 = 1
            ch2 = 2
        elif (IN1_pin == pyb.Pin(pyb.Pin.cpu.B5))&(IN2_pin == pyb.Pin(pyb.Pin.cpu.B4)):
            ch1 = 2
            ch2 = 1
        elif (IN1_pin == pyb.Pin(pyb.Pin.cpu.B0))&(IN2_pin == pyb.Pin(pyb.Pin.cpu.B1)):
            ch1 = 3
            ch2 = 4
        elif (IN1_pin == pyb.Pin(pyb.Pin.cpu.B1))&(IN2_pin == pyb.Pin(pyb.Pin.cpu.B0)):
            ch1 = 4
            ch2 = 3
        else:
            print('Unrecognized Pins')
        # Set input timers
        ## Input 1
        self.in1 = timer.channel(ch1,pyb.Timer.PWM, pin = IN1_pin)
        ## Input 2
        self.in2 = timer.channel(ch2,pyb.Timer.PWM, pin = IN2_pin)
    def enable (self):
        '''
        @brief Allows all motors to move
        '''
        self.sleep.high()
    def disable (self):
        '''
        @brief  Cuts all motion
        '''
        self.sleep.low()
        
    def set_duty (self, duty):
        ''' 
        @brief  This method sets the duty cycle to be sent to the motor to the 
        given level. Positive values cause effort in one direction, negative 
        values30in the opposite direction.
        @param duty     A signed integer holding the duty cycle of the PWM signal sent to the motor 
        '''
        ma = 80
        mi = -80
        
        if duty < 0:
            # Negative case
            if duty > mi:
                self.in1.pulse_width_percent(-duty)
                self.in2.pulse_width_percent(0)
            else:
                self.in1.pulse_width_percent(-mi)
                self.in2.pulse_width_percent(0)
        elif duty >= 0:
            # Positive case
            if duty < ma:
                self.in1.pulse_width_percent(0)
                self.in2.pulse_width_percent(duty)
            else:
                self.in1.pulse_width_percent(0)
                self.in2.pulse_width_percent(ma)

if __name__ =='__main__':
    # Adjust the following code to write a test program for your motor class
    #code within the if __name__ == '__main__' block will only run when the
    # script is executed as a standalone program. If the script is imported as
    # a module the code block will not run.
    
    # Create the pin objects used for interfacing with the motor drivers      
    pin_nSLEEP  = pyb.Pin(pyb.Pin.cpu.A15,pyb.Pin.OUT_PP)   
    pin_IN1     = pyb.Pin(pyb.Pin.cpu.B5)      
    pin_IN2     = pyb.Pin(pyb.Pin.cpu.B4)
    pin_IN3     = pyb.Pin(pyb.Pin.cpu.B0)      
    pin_IN4     = pyb.Pin(pyb.Pin.cpu.B1)
    # Create the timer object used for PWM generation
    tim = pyb.Timer(3,freq = 20000);
    # Create motor objects passing in the pins and timer
    moe     = MotorDriver(pin_nSLEEP, pin_IN1, pin_IN2, tim)
    moe2    = MotorDriver(pin_nSLEEP, pin_IN3, pin_IN4, tim)
    # Enable the motor driver
    moe.enable()
    moe2.enable()
    # Set the duty cycle to 80 percent
    moe.set_duty(80)
    moe2.set_duty(80)