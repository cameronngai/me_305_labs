'''
@file  NucleoUI.py
@package NucleoUI
@brief      This object will interface the closed loop motor controller with the
            user
@details    This file contains the Nucleo end of a user interface for a closed loop
            proportional controller. It works in conjunction with UI.py, which 
            manages the interface that the user interacts with. Other files on
            the nucleo include ClosedLoop.py, Encoder.py, and MotorDriver.py. This interaction 
            is shown in the diagram below:
@image html UITask.png

@details    Link to code: https://bitbucket.org/cameronngai/me_305_labs/src/master/Lab6/NucleoUI.py
@author:     Cameron Ngai
@date: November 18, 2020
'''

import pyb
import array
import MotorDriver as mo
import Encoder as enc
from ClosedLoop import ClosedLoop

class NucleoUI:
    '''
    @brief  An object allowing the user to interface the controller
    @details    This object will allow the user to collect data from runs of the
                encoder. The user will set desired velocities and proportional gains
                for the controller to respond to. Then, user will be able to run
                test on the given controller settings. The UI will collect data on these tests.
                The data will be downloaded as both a list and an excell file.
    The finite state machine for this task is as below:
    @image html NucleoUI.png
    '''
    
    # States
    ## Initialisation state
    INIT = 1
    ## State when the interface is doing nothing
    STAND_BY = 2
    ## State when the encoder is collecting data
    COLLECT = 3
    ## State when the interface is sending data
    SEND = 4
    ## State when retrieving the desired velocity
    GET_VEL = 5
    ## State when retrieving the proprotional gain
    GET_KP = 6
    
    SENDT = 7
    
    SENDX = 8
    
    def __init__(self,encoder,Closed_Loop,refresh_rate):
        '''
        @brief  Creates an Nucleo UI object
        @param  encoder     The raw encoder to collect data from
        @param  Closed_Loop     The closed loop controller to interface
        @param  refresh_rate  The rate at which the user interface updates itself
        '''
        # Internalize variables
        ## Raw encoder from which position data is derived
        self.encoder = encoder
        ## The closed loop controller to interface
        self.CL = Closed_Loop
        ## Rate at which the states are checked and refreshed
        self.rate = refresh_rate*10**6
        
        ## Serial connection with computor
        self.uart = pyb.UART(2)
        
        # Initialize time parameters
        ## Timestamp of initialization
        self.init_time = pyb.micros()
        ## Current time of the task
        self.curr_time = self.init_time
        ## Next time at which the state is checked and refreshed
        self.refresh_time = self.curr_time+self.rate
        
        # Set initial state
        ## State of the task
        self.state = self.INIT
        
        # LED signals when data is being collected
        # Pin for the LED
        pin5 = pyb.Pin(pyb.Pin.cpu.A5)
        # Timer for the LED signal
        t = pyb.Timer(2,freq = 20000)
        ## signal for the LED
        self.led = t.channel(1,pyb.Timer.PWM,pin = pin5)
        ## Output of the LED
        self.out = 0
        
        ## Desired velocity
        self.vel  = -15
        ## Proportional gain
        self.Kp = 4
        
    def run(self):
        '''
        @brief Runs the interface
        '''
        # Set current time
        self.curr_time = pyb.micros()
        
        # Check for refresh time
        if self.curr_time >= self.refresh_time:
            # Update refresh time
            self.refresh_time += self.rate
            # Check for initial state
            
            if self.state == self.INIT:
                self.state = self.STAND_BY
                self.out = 0
            elif self.state == self.STAND_BY:
                # Value sent to the interface from the serial
                val = self.get_command()
                if val == ord('g'):
                    # Begin collection
                    
                    # Zero encoder
                    self.encoder.set_position(0)
                    
                    # Set next state
                    self.state = self.COLLECT
                    
                    # Turn LED on
                    self.out = 1
                    
                    # Initiate arrays
                    ## Array of timestamps for the positions
                    self.tout = array.array('L',[])
                    ## Array of positions
                    self.xout = array.array('H',[])
                    
                    self.CL.set_vel(self.vel)
                    self.CL.set_Kp(self.Kp)
                    
                    # Set start time
                    ## Time at which collection begins
                    self.start_time = self.curr_time
                    
                elif val == ord('v'):
                    # Get velocity
                    self.state = self.GET_VEL
                elif val == ord('k'):
                    # Get proportional gain
                    self.state = self.GET_KP
                    
            elif self.state == self.GET_VEL:
                # Get number
                val = self.get_numb()
                if val != None:
                    if val == 'n':
                        self.state = self.STAND_BY
                    else:
                        self.vel = val
                        self.state = self.STAND_BY
            elif self.state == self.GET_KP:
                # Get number
                val = self.get_numb()
                if val != None:
                    if val == 'n':
                        self.state = self.STAND_BY
                    else:    
                        self.Kp = val
                        self.state = self.STAND_BY
                    
            elif self.state == self.COLLECT:
                # Update arrays with data
                val = self.get_command()
                self.encoder.update
                self.tout.append(self.curr_time-self.start_time)
                self.xout.append(self.encoder.get_position())
                
                # Quit if stopped
                if val == ord('s'):
                    ## Index of array object to send
                    self.i = 0
                    self.CL.set_vel(0)
                    self.out = 0
                    self.state = self.SENDT ## This was origionally just send
        # if self.state == self.SEND:
        #     val = self.get_command()
        #     if val == ord('t'):
        #         # Send tout array data
        #         self.send_array(self.tout,'L')
        #         if self.i >= len(self.tout):
        #             self.i = 0
        #     if val == ord('x'):
        #         # Send xout array data
        #         self.send_array(self.xout,'H')
        #         if self.i >= len(self.xout):
        #             # Finish collection
        #             self.i = 0
        #             #self.out = 0
        #             del(self.tout)
        #             del(self.xout)
        #             self.state = self.INIT
                    
        if self.state == self.SENDT:
            #val = self.get_command()
            #if val == ord('t'):
                # Send tout array data
            self.send_array(self.tout,'L')
            if self.i >= len(self.tout):
                self.i = 0
                del(self.tout)
                self.state = self.SENDX
                
        elif self.state == self.SENDX:
            self.out = 1
            self.send_array(self.xout,'H')
            if self.i >= len(self.xout):
            # Finish collection
                self.i = 0
                #self.out = 0
                del(self.xout)
                self.state = self.INIT
        

        self.CL.run()
        
        # Run LED
        self.led.pulse_width_percent(100*self.out)
        
    def get_command(self):
        '''
        @brief  gets command through serial    
        '''
        if self.uart.any() != 0:
            return self.uart.readchar()
        else:
            return ''
    def send_array(self, arr,typ):
        '''
        @brief  Sends array object through serial
        @param  arr     The array sent through the serial
        @param  tye     The type of array object
        '''
        # Assess if the object is the last object in the array
        if self.i == len(arr)-1:
            end = 1
        else:
            end = 0
            
        ## Bytearray representing object in array to send through the serial
        b = bytearray(array.array(typ,[arr[self.i]]))+bytearray([end])
        
        # Submit bytearray and reindex
        #if len(b) != 5:
        #    self.out = 0
        self.uart.write(b)
        self.i += 1 #n

    def get_numb(self):
        '''
        @brief Retrieves a number through serial
        '''
        if self.uart.any() != 0:
             size1 = self.uart.any()
             pyb.udelay(int(1/115273*15*10**6))
             size2 = self.uart.any()
             # Check if the system is done sending
             if size1 == size2:
                 ## Raw value recieved through bluetooth
                 string = self.uart.read(self.uart.any()).decode('ASCII')
                 if string == 'n':
                     return 'n'
                 else:
                     return float(string)
        else:
             return None


if __name__ == '__main__':
    '''
    @brief  Runs an instance of the encoder interface for testing
    '''
    
    interval = 0.01
    # Create the pin objects used for interfacing with the motor drivers      
    pin_nSLEEP  = pyb.Pin(pyb.Pin.cpu.A15,pyb.Pin.OUT_PP)   
    pin_IN1     = pyb.Pin(pyb.Pin.cpu.B5)      
    pin_IN2     = pyb.Pin(pyb.Pin.cpu.B4)
    
    # Create the timer object used for PWM generation
    tim = pyb.Timer(3,freq = 20000)
    # Create motor objects passing in the pins and timer
    moe     = mo.MotorDriver(pin_nSLEEP, pin_IN1, pin_IN2, tim)
    # Enable the motor driver
    moe.enable()
    
    timer = 4
    ## First encoder input pin
    pinA = pyb.Pin.cpu.B6  # Digital 10
    ## Second encoder input pin
    pinB = pyb.Pin.cpu.B7
    ## Raw encoder to collect data from
    re = enc.Raw_Encoder(timer,pinA,pinB)
    ## Encoder to use as feedback
    e = enc.Encoder(re,interval)
    
    ## Closed Loop controller
    CL = ClosedLoop(e,moe,interval)
    
    ## Interface on the nucleo
    ui = NucleoUI(re,CL,interval)
    
    # Run program
    while True:
        ui.run()