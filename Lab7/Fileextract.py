# -*- coding: utf-8 -*-
"""
Created on Wed Nov 25 10:14:56 2020

@author: camer
"""

import numpy as np

myFile = np.genfromtxt('reference.csv', delimiter=',')
t = myFile[:,0]
vel = myFile[:,1]
pos = myFile[:,2]
