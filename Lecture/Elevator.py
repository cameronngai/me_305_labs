r'''
@file Elevator.py

@author Cameron Ngai

@date October 6, 2020

@brief  This file will implement some code to simulate an imaginary elevator

@details    Three objects are involved: the elevator controller, a simulated elevator,
and a simulated user. In practice, only the elevator controller would be used,
as the elevator and user would be real.

The following is a link to the code: https://bitbucket.org/cameronngai/me_305_labs/src/master/Lecture/Elevator.py

The finite state machine for the elevator controller is as sketched below:
@image html fsmlec.png
'''

from random import choice
import time

class TaskElevator:
    r'''
    @brief      A finite state machine to control elevators.
    @details    This class implements a finite state machine to control the
                operation of elevators that run between two floors
                The finite state machine can be seen below:
    @image html fsmlec.png
    
    '''
    
    # Constants defining elevator states
    ## Initialization state
    S0_INIT = 0
    ## State when elevator is moving down
    MOVING_DOWN = 1
    ## State when elevator is moving up
    MOVING_UP = 2
    ## State when elevator is stopped at floor 1
    STOPPED_FLOOR_1 = 3
    ## State when elevator is stopped at floor 2
    STOPPED_FLOOR_2 = 4
    
    # Constants defining input and output states
    ## Constant representing the motor lowering the elevator
    MotorDown = 2
    ## Constant representing the motor raising the elevator
    MotorUp = 1
    ## Constant representing the motor maintaining the elevator position
    MotorStop = 0
    ## Constant representing a button being on
    ButtonOn = 0
    ## Constant representing a button being off
    ButtonOff = 1
    
    def __init__(self,interval):
        '''
        @brief      Creates a TaskElevator object.
        @param interval  An object that represents the time interval between actions
        '''
        
        # The state to run on the next iteration of the task.
        ## The state of the elevator
        self.state = self.S0_INIT
        
        # The interval of time between task action
        ## An object that represents the time interval between actions
        self.interval = interval
        
        # Settup initial time
        ## Initial time of elevator use
        self.init_time = time.time()
        ## Current time of elevator use
        self.curr_time = self.init_time
        
        # Set next task action time
        ## Time at which the the microcontrollor will check for conditions
        ## necessary for changing states
        self.next_time = self.init_time + self.interval
        
        # Set motor to 0. This is only needed for elevator.track to run before
        # state 0 of this code is implemented
        ## An object representing the state of the motor
        self.motor = self.MotorStop
        
    def run(self):
        '''
        @brief      Runs one iteration of the task
        '''
        
        # Update time
        self.curr_time = time.time()
        
        # Run when time interval between action is completed
        if (self.curr_time > self.next_time):
            
            # Set next task action time
            self.next_time = self.next_time+self.interval
            
            if(self.state == self.S0_INIT):
                # Elevator starts up. The moter is set to move down
                
                self.transitionTo(self.MOVING_DOWN)
                self.motor = self.MotorDown
                ## An object representing the state of the down button
                self.Button_1 = self.ButtonOff
                ## An object representing the state of the up button
                self.Button_2 = self.ButtonOff
                
                # Print activity and timestamp
                print('The elevator starts up. t: '+str(self.curr_time-self.init_time))
        
                
            elif(self.state == self.MOVING_DOWN):
                # Elevator moves down untill floor 1 is reached. Then,
                # the motor will stop and the buttons will be refreshed
                ## An object representing the sensor input detecting the elevator's
                ## position at the first floor
                if(self.first == True):
                    
                    self.transitionTo(self.STOPPED_FLOOR_1)
                    self.motor = self.MotorStop
                    self.Button_1 = self.ButtonOff
                    self.Button_2 = self.ButtonOff
                    
                    # Print activity and timestamp
                    print("The elevator reaches floor 1. t: "+str(self.curr_time-self.init_time))

            elif(self.state == self.MOVING_UP):
                # Elevator moves down untill floor 1 is reached. Then,
                # the motor will stop and the buttons will be refreshed
                ## An object representing the sensor input detecting the elevator's
                ## position at the second floor
                if(self.second == True):
                    
                    self.transitionTo(self.STOPPED_FLOOR_2)
                    self.motor = self.MotorStop
                    self.Button_1 = self.ButtonOff
                    self.Button_2 = self.ButtonOff
                    
                    # Print activity and timestamp
                    print("The elevator reaches floor 2. t: "+str(self.curr_time-self.init_time))

            elif(self.state == self.STOPPED_FLOOR_1):
                # Elevator is at floor 1. It will move up if the floor 2 button
                # is pressed
                if(self.Button_2 == self.ButtonOn):
                    
                    self.transitionTo(self.MOVING_UP)
                    self.motor = self.MotorUp
                    
                    # Print activity and timestamp
                    print("The floor 2 button is pressed. t: "+str(self.curr_time-self.init_time)+"\nThe elivator is acending. t: "+str(self.curr_time-self.init_time))
                    
            elif(self.state == self.STOPPED_FLOOR_2):
                # Elevator is at floor 2. It will move down if the floor 1 button
                # is pressed
                
                if(self.Button_1 == self.ButtonOn):
                    self.transitionTo(self.MOVING_DOWN)
                    self.motor = self.MotorDown
                    
                    ## Print activity and timestamp
                    print("The floor 1 button is pressed. t: "+str(self.curr_time-self.init_time)+ "\nThe elivator is decending. t: "+str(self.curr_time-self.init_time))

            #print(str(self.runs)+': run '+str(self.state) +' '+ str(self.curr_time-self.init_time))
    def transitionTo(self, newState):
        '''
        @brief      Updates the variable defining the next state to run
        '''
        self.state = newState
        


class User:
    '''
    @brief      An elevator user class
    @details    This class represents an elevator user. The user will enter the elevator,
                push the button after a time dependent on the user's temperment,
                ride the elevator down, and leave after a similar time.
    '''
    
    # State definitions
    ## State of getting on the elevator
    GETTING_ON = 0
    ## State of waiting for the elevator to reach it's destination
    RIDING = 1
    ## State of getting off of the elevator
    GETTING_OFF = 2
    ## State representing any activity not involving the elevator. This also serves as
    ## the initialization state
    NOT_ON = 3
    
    def __init__(self):
        '''
        @brief      Creates a User
        '''
        
        ## The user's disposition is randomly assigned. The disposition is how
        ## long the user takes to press a button after entering and how long
        ## to leave after reaching the destination
        self.disposition = choice([1,2,3,4])
        
        # The user is set to not being on the elevator
        ## The state is the state that the user is at
        self.state = self.NOT_ON
        
        # The user's initial time is set
        ## The init_time variable marks the time after each state transition
        self.init_time = time.time()
        ## The current time is the time that the user experiences
        self.curr_time = self.init_time
        
    def ride(self,elevator):
        '''
        @brief      Accounts all activities of the user when on the elevator
        @param elevator  The elevator that the user is riding 
        '''
        
        # Time is updated
        self.curr_time = time.time()
        
        if (self.state == self.GETTING_ON):
            # The user gets on the elevator and presses the button for the
            # floor he is not on after that is in accordance with the user's
            # disposition
            if (self.curr_time >= self.init_time+self.disposition):
                self.transitionTo(self.RIDING)
                
                if (elevator.control.second == True):
                    elevator.control.Button_1 = elevator.control.ButtonOn
                    ## The destination is the floor that the user wishes to go to
                    self.destination = elevator.control.STOPPED_FLOOR_1
                elif (elevator.control.first == True):
                    elevator.control.Button_2 = elevator.control.ButtonOn
                    self.destination = elevator.control.STOPPED_FLOOR_2
                    
                # update reference time
                self.init_time = time.time()
                    
        elif (self.state == self.RIDING):
            # The user waits in the elevator as until the destination is reached
            if (elevator.control.state == self.destination):
                
                self.transitionTo(self.GETTING_OFF)
                
                # update reference time
                self.init_time = time.time()
                
        elif (self.state == self.GETTING_OFF):
            # The user gets off the elevator at after a time in accordance with
            # the user's disposition
            if (self.curr_time >= self.init_time+self.disposition):
                
                self.transitionTo(self.NOT_ON)
                elevator.occupied = False
                
                # Print activity and timestamp
                print('The user exits the elevator. t: '+str(elevator.control.curr_time-elevator.control.init_time))
                
                # update reference time
                self.init_time = time.time()
                
    def transitionTo(self, newState):
        '''
        @brief      Updates the variable defining the next state to run
        '''
        self.state = newState
        
        

class ElevatorSim:
    '''
    @brief      A simulation of the elevator
    @details    This class is a simulation for an elevator. It tracks the
                position and vacancy of the elevator, and contains the relivant
                controller.
    '''
    
    ## The first floor is set to a position of zero floors abouve ground
    Floor_1 = 0
    ## The second floor is set to a position of one floor abouve ground
    Floor_2 = 1
    
    def __init__(self,interval,init_pos,speed,control):
        '''
        @brief Creates an elevator simulation
        @param interval  The time interval between simulation update
        @param init_pos  The initial position of the elevator in floors abouve ground
        @param speed  The speed of the elevator in floors/second
        @param control  The controller for the elevator, as represented by a TaskElevator object
        '''
        
        # Set parameters to object
        ## The time interval between simulation update
        self.interval = interval
        ## The position of the elevator in floors abouve ground
        self.pos = init_pos
        ## The speed of the elevator in floors/second
        self.speed = speed
        ## The controller for the elevator, as represented by a TaskElevator object
        self.control = control
        
        # Elevator begins unoccupied
        ## A boolian representing whether or not the elevator is occupied
        self.occupied = False
        
        # Set initial time
        ## Initial time of the simulation
        self.init_time = time.time()
        ## Current time of the simulation
        self.curr_time = self.init_time
        
        # Set next update time   
        ## Next_time is the time at which the elevator will update it's position and sensor values
        self.next_time = self.init_time + self.interval
    
    def track(self):
        '''
        @brief Tracks the elevator position
        '''
        
        # Get current time
        self.curr_time = time.time()
        
        # Update simulation when update timestame is reached
        if (self.curr_time > self.next_time):
            
            ## Set next update time
            self.next_time = self.next_time+self.interval
            
            ## Move elevator in accordance with motor at specified speed
            if (self.control.motor == self.control.MotorDown):
                self.pos -= self.speed*self.interval
            elif (self.control.motor == self.control.MotorUp):
                self.pos += self.speed*self.interval
            
        # Check for presence at first or second floor
        if (self.pos >= self.Floor_2):
            self.control.second = True
            self.pos = self.Floor_2
        elif (self.pos <= self.Floor_1):
            self.control.first = True
            self.pos = self.Floor_1
        else:
            self.control.second = False
            self.control.first = False
        
        
        
if __name__ == '__main__':
    '''
    @brief      Runs a simulation of an elevvator
    @details    This is a test for the code. It runs a simulation where a user
                enters, rides, and exits the elevator at random times.
    '''
    
    # Create elevator simulation
    interval = 0.01
    init_pos = 0.1
    speed = 0.25
    control_interval = 0.1
    elevator = ElevatorSim(interval,init_pos,speed,TaskElevator(control_interval))
    
    # Create User
    new_user = User()
    
    # Run Simulation
    sim_time = 60
    while (elevator.curr_time < elevator.init_time+sim_time):
        
        # Run tasks
        elevator.control.run()
        elevator.track()
        new_user.ride(elevator)
        
        # Elevator accessabillity
        elevator_at_floor = (elevator.control.state == elevator.control.STOPPED_FLOOR_1) | (elevator.control.state == elevator.control.STOPPED_FLOOR_2)
        user_present = choice([0]*30000+[1])
        
        if ((elevator.occupied == False) & elevator_at_floor & user_present):
            # User enters when available
            
            new_user.state = new_user.GETTING_ON
            new_user.init_time = time.time()
            elevator.occupied = True
            
            # Print activity and timestamp
            print('A new user enters the elevator. t: '+str(elevator.control.curr_time-elevator.control.init_time))
            
                
    
    
