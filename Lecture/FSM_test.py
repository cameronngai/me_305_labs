# -*- coding: utf-8 -*-
"""
Created on Fri Oct  2 10:43:40 2020

@author: camer
"""

from FSM_example import Button, MotorDriver, TaskWindshield

GoButton = Button('PB6')
LeftLimit = Button('PB7')
RightLimit = Button('PB8')
Motor = MotorDriver()

task1 = TaskWindshield(0.1,GoButton,LeftLimit,RightLimit,Motor)

while task1.curr_time < task1.init_time+5:
    task1.run()
    