# -*- coding: utf-8 -*-
"""
Created on Fri Oct 23 10:14:44 2020

@author: camer
"""
import numpy as np

import matplotlib as mpl

def getNext(x,xdot,dt,func):
    xn = x+xdot*dt+1/2*func(x,xdot)*dt**2
    xdotn = xdot+1/2*(func(x,xdot)+func(xn,xdot))*dt
    return (xn,xdotn)

class difEq:
    def __init__(self,k,a):
        self.k = k
        self.a = a
    def evaluate(self,x,xdot):
        return -self.k*x-self.a*x**3

def solve(difEq, x0,xdot0, tspan,dt):
    l = int((tspan[1]-tspan[0])/dt)
    tout = np.linspace(tspan[0],tspan[1],l)
    x = np.zeros(l)
    xdot = np.zeros(l)
    x[0] = x0
    xdot[0] = xdot0
    for i in range(1,l):
        (x[i],xdot[i]) = getNext(x[i-1],xdot[i-1],dt,difEq.evaluate)
    return (tout,x)    
        
if __name__ == '__main__':
    k = 2
    a = 5
    
    deq = difEq(k,a)
    
    l = int(1/0.1)
    x0array = np.linspace(0,1,l)
    xdot0 = 0
    tspan = (0,10)
    dt = 0.1
    
    tout = [0]*l
    x = [0]*l
    
    for i in range(0,l):
        x0 = x0array[i]
        (tout[i],x[i]) = solve(deq,x0,xdot0,tspan,dt)
        mpl.pyplot.plot(tout[i],x[i])
    
    mpl.pyplot.xlabel('time, t [s]')
    mpl.pyplot.ylabel('position, x [m]')
    
    
    
        
        